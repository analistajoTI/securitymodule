# README #

**El modulo de seguridad es un proyecto de servicios en springboot 
**que nos permite tener validacion de seguridad por medio de bearer token y 
**tambien nos permite la gestion de los roles de usuarios.

### Environment ###

**JAVA 1.8.0.251 
**TOMCAT 9.0.37
**SPRINGBOOT 2.3.2.RELEASE
** HANA DB 1.00.122.27.1568902538
** SQLSERVER 2018

### How do I get set up? ###

**click derecho en el proyecto despues de que nuestro maven descargue todas las dependencias necesarias, luego dar click en la opcion run as y seleccionar nuestro servidor de aplicaciones e ingresar por medio de postman o nuestro navegador web a uno de los links que dejaremos al final del readme, tambien por medio del archivo properties se puede seleccionar la conexion que deseamos comentando la de hana y poniendo la se sqlserver o viceversa.

### Contribution guidelines ###

**Para obtener el token  

http://localhost:8080/securitymodule-0.0.1-SNAPSHOT/auth/signin
servicio de tipo POST 

En el body 
{
	"username": "ADMIN",
	"password": "1234"
}

**Para verificar el uso de roles servicios de tipo GET

ADMIN
**http://localhost:8080/securitymodule-0.0.1-SNAPSHOT/alvaro/mensaje
**http://localhost:8080/securitymodule-0.0.1-SNAPSHOT/alvaro/gestor
**http://localhost:8080/securitymodule-0.0.1-SNAPSHOT/alvaro/mensaje2
**http://localhost:8080/securitymodule-0.0.1-SNAPSHOT/alvaro/admin

USER 

**http://localhost:8080/securitymodule-0.0.1-SNAPSHOT/alvaro/mensaje
**http://localhost:8080/securitymodule-0.0.1-SNAPSHOT/alvaro/mensaje2

GUESS

**http://localhost:8080/securitymodule-0.0.1-SNAPSHOT/alvaro/gestor


### Who do I talk to? ###

* Coordinador TI
* Analista TI