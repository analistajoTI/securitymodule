package com.jo.webservices;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;
import org.springframework.context.annotation.ComponentScan;

import com.jo.webservices.db.SapDB;

@SpringBootApplication
@ComponentScan(basePackageClasses = JOWebServicesApp.class, basePackages = "com.jo.webservices")
public class JOWebServicesApp extends SpringBootServletInitializer {

	@Override
	protected SpringApplicationBuilder configure(SpringApplicationBuilder application) {
		return application.sources(JOWebServicesApp.class);
	}

	public static void main(String[] args) {
		SapDB.ip = "10.126.10.5";
		SapDB.schemaDB = "SBOJOPRUEBASTI";
		SapDB.port = "30015";
		SapDB.userDB = "SYSTEM";
		SapDB.password = "JohaOrtiz2020#";
		
		SpringApplication.run(JOWebServicesApp.class, args);
	}

}
