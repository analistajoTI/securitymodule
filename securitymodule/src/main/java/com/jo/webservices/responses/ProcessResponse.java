package com.jo.webservices.responses;

public class ProcessResponse {

	private boolean success;

	private String message;

	public ProcessResponse() {
		super();
	}

	public ProcessResponse(boolean success, String message) {
		super();
		this.success = success;
		this.message = message;
	}

	public boolean isSuccess() {
		return success;
	}

	public void setSuccess(boolean success) {
		this.success = success;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

}
