package com.jo.webservices.db;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import com.jo.webservices.models.Roles;

public class SapDB {

	public static String ip;
	public static String port;
	public static String schemaDB;
	public static String userDB;
	public static String password;

	private static Connection connection() throws SQLException, ClassNotFoundException {
		
		SapDB.ip = "10.126.10.5";
		SapDB.schemaDB = "SBOJOPRUEBASTI";
		SapDB.port = "30015";
		SapDB.userDB = "SYSTEM";
		SapDB.password = "JohaOrtiz2020#";

		Class.forName("com.sap.db.jdbc.Driver");
		Connection connection = null;
		if (schemaDB != null) {
			connection = DriverManager.getConnection(
					"jdbc:sap://" + ip + ":" + port + "/?currentschema=" + schemaDB + "&autocommit=false", userDB,
					password);
		} else {
			connection = DriverManager.getConnection("jdbc:sap://" + ip + ":" + port + "/?autocommit=false", userDB,
					password);
		}
		return connection;
	}

	public static List<Roles> getAllPermissions() throws SQLException, ClassNotFoundException {
		Connection conn = connection();
		String sql = "SELECT \"PERMISSION\" FROM \"PERMISSION\"";
		PreparedStatement ps = conn.prepareStatement(sql);
		ResultSet rs = ps.executeQuery();
		List<Roles> listReturn = new ArrayList<Roles>();
		while (rs.next()) {
			Roles obj = new Roles();
			obj.setPermission(rs.getString("PERMISSION"));

			listReturn.add(obj);
		}
		rs.close();
		ps.close();
		conn.close();
		return listReturn;
	}

	public static String[] getRolesForPermission(String Permission) throws SQLException, ClassNotFoundException {
		Connection conn = connection();
		String sql = "SELECT SUBSTR(T1.\"COD_ROL\",6) AS \"COD_ROL\" FROM \"PERMISSION\" T0 INNER JOIN \"ROL_PERMISSION\" T1 ON T0.\"CODE\" = T1.\"COD_PER\" WHERE T0.\"PERMISSION\" = ?;";
		PreparedStatement ps = conn.prepareStatement(sql);
		ps.setString(1, Permission);
		ResultSet rs = ps.executeQuery();
		List<String> lista = new ArrayList<String>();
		while (rs.next()) {
			String rol = "";
				rol = rs.getString("COD_ROL");
			lista.add(rol);
		}
		rs.close();
		ps.close();
		conn.close();
		return lista.toArray(new String[lista.size()]);
	}

}
