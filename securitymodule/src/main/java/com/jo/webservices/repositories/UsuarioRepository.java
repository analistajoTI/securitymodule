package com.jo.webservices.repositories;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import com.jo.webservices.models.Usuario;

public interface UsuarioRepository extends CrudRepository<Usuario, String> {

	@Query("SELECT e FROM Usuario e WHERE e.username = :usuario")
	public Usuario findByUsername(@Param("usuario") String usuario);

}
