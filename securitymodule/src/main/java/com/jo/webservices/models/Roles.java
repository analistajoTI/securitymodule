package com.jo.webservices.models;

public class Roles {

	private String rol;
	private String permission;
	
	
	public Roles() {
		super();
	}


	public String getRol() {
		return rol;
	}


	public void setRol(String rol) {
		this.rol = rol;
	}


	public String getPermission() {
		return permission;
	}


	public void setPermission(String permission) {
		this.permission = permission;
	}


	@Override
	public String toString() {
		return "Roles [rol=" + rol + ", permission=" + permission + "]";
	}
	
	
	
}
