package com.jo.webservices.controllers;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class PruebaController {
	
	
	@RequestMapping(method = RequestMethod.GET, path ="/alvaro/mensaje")
	public String mensaje(){
		
		return "servicio";
	}
	
}
