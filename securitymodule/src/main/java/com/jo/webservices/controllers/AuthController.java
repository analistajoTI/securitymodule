package com.jo.webservices.controllers;

import java.util.HashMap;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.jo.webservices.models.Usuario;
import com.jo.webservices.requests.AuthenticationRequest;
import com.jo.webservices.security.JwtTokenProvider;

@RestController
@RequestMapping("/auth")
public class AuthController {

	@Autowired
	private AuthenticationManager authenticationManager;

	@Autowired
	private JwtTokenProvider jwtTokenProvider;


	@PostMapping("/signin")
	public ResponseEntity<Map<String, Object>> signin(@RequestBody AuthenticationRequest data) {
		try {
			Authentication authentication = authenticationManager
					.authenticate(new UsernamePasswordAuthenticationToken(data.getUsername(), data.getPassword()));
			Usuario user = (Usuario) authentication.getPrincipal();
			String token = jwtTokenProvider.createToken(data.getUsername(),user.getAuthorities());
			Map<String, Object> model = new HashMap<>();
			model.put("codigo", user.getCodigo());
			model.put("nombre", user.getNombre());
			model.put("username", data.getUsername());
			model.put("token", token);
			return new ResponseEntity<>(model, HttpStatus.OK);
		} catch (AuthenticationException e) {
			throw new BadCredentialsException("Invalid username/password supplied");
		}
	}

}