package com.jo.webservices.controllers;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class PruebaController2 {
	
	
	@RequestMapping(method = RequestMethod.GET, path ="/alvaro/mensaje2")
	public String mensaje(){
		
		return "servicio2";
	}
	
}
