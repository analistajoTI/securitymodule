package com.jo.webservices.controllers;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class GestorController3 {
	
	
	@RequestMapping(method = RequestMethod.GET, path ="/alvaro/gestor")
	public String mensaje(){
		
		return "GESTOR";
	}
	
}
