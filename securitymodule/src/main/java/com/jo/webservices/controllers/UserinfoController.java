package com.jo.webservices.controllers;

import java.util.HashMap;
import java.util.Map;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import com.jo.webservices.models.Usuario;



@RestController
public class UserinfoController {
	
	@GetMapping("/auth/currentUser")
	public ResponseEntity<?> currentUser(@AuthenticationPrincipal Usuario userDetails) {
		Map<Object, Object> model = new HashMap<>();
		model.put("codigo", userDetails.getCodigo());
		model.put("nombre", userDetails.getNombre());
		model.put("username", userDetails.getUsername());
		model.put("roles", userDetails.getAuthorities());		
		return new ResponseEntity<>(model, HttpStatus.OK);
	}
	
}