package com.jo.webservices.security;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.UnsupportedEncodingException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.security.spec.KeySpec;

import javax.crypto.Cipher;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.SecretKey;
import javax.crypto.SecretKeyFactory;
import javax.crypto.spec.DESKeySpec;
import javax.crypto.spec.DESedeKeySpec;

import sun.misc.BASE64Decoder;
import sun.misc.BASE64Encoder;

@SuppressWarnings("restriction")
public class StringEncrypter {

	public static final String DESEDE_ENCRYPTION_SCHEME = "DESede";

	public static final String DES_ENCRYPTION_SCHEME = "DES";

	public static final String DEFAULT_ENCRYPTION_KEY = "This is a fairly long phrase used to encrypt";

	private KeySpec keySpec;

	private SecretKeyFactory keyFactory;
	
	public static String KEY="1617479120710894180105212232798";

	private Cipher cipher;

	private static final String UNICODE_FORMAT = "UTF8";

	public StringEncrypter(String encryptionScheme) throws EncryptionException {
		this(encryptionScheme, DEFAULT_ENCRYPTION_KEY);
	}

	public static void main (String[]args){
		try {
			StringEncrypter stringEnripter=new StringEncrypter(DES_ENCRYPTION_SCHEME,KEY);
			String encriptedPassword=stringEnripter.encrypt("1041890932");
			System.out.println("encripted : " + encriptedPassword);
			String decripted=stringEnripter.decrypt(encriptedPassword);
			System.out.println("decripted : " + decripted);
			
		} catch (EncryptionException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}
	
	public StringEncrypter(String encryptionScheme, String encryptionKey)
			throws EncryptionException {

		if (encryptionKey == null)
			throw new IllegalArgumentException("encryption key was null");
		if (encryptionKey.trim().length() < 24)
			throw new IllegalArgumentException(
					"encryption key was less than 24 characters");

		try {
			byte[] keyAsBytes = encryptionKey.getBytes(UNICODE_FORMAT);

			if (encryptionScheme.equals(DESEDE_ENCRYPTION_SCHEME)) {
				keySpec = new DESedeKeySpec(keyAsBytes);
			} else if (encryptionScheme.equals(DES_ENCRYPTION_SCHEME)) {
				keySpec = new DESKeySpec(keyAsBytes);
			} else {
				throw new IllegalArgumentException(
						"Encryption scheme not supported: " + encryptionScheme);
			}

			keyFactory = SecretKeyFactory.getInstance(encryptionScheme);
			cipher = Cipher.getInstance(encryptionScheme);

		} catch (InvalidKeyException e) {
			throw new EncryptionException(e);
		} catch (UnsupportedEncodingException e) {
			throw new EncryptionException(e);
		} catch (NoSuchAlgorithmException e) {
			throw new EncryptionException(e);
		} catch (NoSuchPaddingException e) {
			throw new EncryptionException(e);
		}

	}

	public String encrypt(String unencryptedString) throws EncryptionException {
		if (unencryptedString == null || unencryptedString.trim().length() == 0)
			throw new IllegalArgumentException(
					"unencrypted string was null or empty");

		try {
			
			SecretKey key = keyFactory.generateSecret(keySpec);
			cipher.init(Cipher.ENCRYPT_MODE, key);
			byte[] cleartext = unencryptedString.getBytes(UNICODE_FORMAT);
			byte[] ciphertext = cipher.doFinal(cleartext);

			BASE64Encoder base64encoder = new BASE64Encoder();
			return base64encoder.encode(ciphertext);
		} catch (Exception e) {
			throw new EncryptionException(e);
		}
	}
	
	@SuppressWarnings("resource")
	public void encryptFile(String fileIn, String fileOut) throws EncryptionException {
		try {
			InputStream archivo = new FileInputStream(fileIn);
			OutputStream fich_out = new FileOutputStream (fileOut);
			
			byte[] buffer = new byte[1024];
			byte[] bloque_cifrado;
			String textoCifrado = new String();
			int fin_archivo = -1;
			int leidos;//numero de bytes leidos
			
			leidos = archivo.read(buffer);
			SecretKey key = keyFactory.generateSecret(keySpec);
			cipher.init(Cipher.ENCRYPT_MODE, key);
			while( leidos != fin_archivo ) {
				bloque_cifrado = cipher.update(buffer,0,leidos);
				textoCifrado = textoCifrado + new String(bloque_cifrado,"ISO-8859-1"); 
				leidos = archivo.read(buffer);          
			}
			
			archivo.close();
			
			bloque_cifrado = cipher.doFinal();
			textoCifrado = textoCifrado + new String(bloque_cifrado,"ISO-8859-1");
			
			fich_out.write(textoCifrado.getBytes("ISO-8859-1"));
		} catch (Exception e) {
			throw new EncryptionException(e);
		}
	}

	public String decrypt(String encryptedString) throws EncryptionException {
		if (encryptedString == null || encryptedString.trim().length() <= 0)
			throw new IllegalArgumentException(
					"encrypted string was null or empty");

		try {
			SecretKey key = keyFactory.generateSecret(keySpec);
			//System.out.println(myKey);
			cipher.init(Cipher.DECRYPT_MODE, key);
			BASE64Decoder base64decoder = new BASE64Decoder();
			byte[] cleartext = base64decoder.decodeBuffer(encryptedString);
			byte[] ciphertext = cipher.doFinal(cleartext);

			return bytes2String(ciphertext);
		} catch (Exception e) {
			throw new EncryptionException(e);
		}
	}
	
	@SuppressWarnings("resource")
	public void decryptFile(String fileIn, String fileOut) throws EncryptionException {
		try {
			InputStream archivo = new FileInputStream(fileIn);
			OutputStream fich_out = new FileOutputStream (fileOut);
			
			byte[] buffer = new byte[1024];
			byte[] bloque_cifrado;
			String textoCifrado = new String();
			int fin_archivo = -1;
			int leidos;//numero de bytes leidos
			
			leidos = archivo.read(buffer);
			SecretKey key = keyFactory.generateSecret(keySpec);
			cipher.init(Cipher.DECRYPT_MODE, key);
			while( leidos != fin_archivo ) {
				bloque_cifrado = cipher.update(buffer,0,leidos);
				textoCifrado = textoCifrado + new String(bloque_cifrado,"ISO-8859-1"); 
				leidos = archivo.read(buffer);          
			}
			
			archivo.close();
			
			bloque_cifrado = cipher.doFinal();
			textoCifrado = textoCifrado + new String(bloque_cifrado,"ISO-8859-1");
			
			fich_out.write(textoCifrado.getBytes("ISO-8859-1"));
		} catch (Exception e) {
			throw new EncryptionException(e);
		}
	}

	private static String bytes2String(byte[] bytes) {
		StringBuffer stringBuffer = new StringBuffer();
		for (int i = 0; i < bytes.length; i++) {
			stringBuffer.append((char) bytes[i]);
		}
		return stringBuffer.toString();
	}

	@SuppressWarnings("serial")
	public static class EncryptionException extends Exception {
		public EncryptionException(Throwable t) {
			super(t);
		}
	}
}