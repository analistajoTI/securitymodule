package com.jo.webservices.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import com.jo.webservices.models.Usuario;
import com.jo.webservices.repositories.UsuarioRepository;

@Service
public class UsuarioServices implements UserDetailsService {

	@Autowired
	private UsuarioRepository repository;

	@Autowired
	private PasswordEncoder passwordEncoder;

	public List<Usuario> findAll() {
		return (List<Usuario>) repository.findAll();
	}

	public void create(Usuario entity) {
		entity.setPassword(passwordEncoder.encode(entity.getPassword()));
		repository.save(entity);
	}

	public boolean update(Usuario entity) {
		if (repository.existsById(entity.getUsername())) {
			entity.setPassword(passwordEncoder.encode(entity.getPassword()));
			repository.save(entity);
			return true;
		} else {
			return false;
		}
	}

	public void delete(Usuario entity) {
		repository.delete(entity);
	}

	@Override
	public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
		Usuario user = repository.findByUsername(username);
		if (user == null) {
			throw new UsernameNotFoundException("Username: " + username + " not found");
		} else {
			return user;
		}

	}
}
